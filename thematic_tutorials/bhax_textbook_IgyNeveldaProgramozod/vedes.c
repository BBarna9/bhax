#include <stdio.h>
#include <stdlib.h>

int main()
{
    int ssz = 5;
    double **hm;

    if((hm = (double **) malloc (ssz * sizeof (double *))) == NULL)
    {
        return -1;
    }

    for (int i = 0; i < ssz; ++i)
    {
        if((hm[i] = (double *) malloc ((i + 1) * sizeof (double))) == NULL)
            return -1;
    }

    for (int i = 0; i < ssz; ++i)
    {
        for (int j = 0; j < i + 1; ++j)
            hm[i][j] = i * (i + 1) / j + 2;
    }

    for (int i = 0; i < ssz; ++i)
    {
        for (int j = 0; j < i + 1; ++j)
            printf("%f, ", hm[i][j]);
        printf("\n");
    }

    for (int i = 0; i < ssz; ++i)
        free (hm[i]);

    free (hm);

    return 0;

}
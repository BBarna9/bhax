#include <iostream>
using namespace std;
class List {
   public:
	List(){
   	   cout << "konstruktor" << endl;
	   head = new ListElem();
	}

	~List(){
	   ListElem* elem = head;
	   while(elem){
		ListElem* akt_elem = elem;
		elem = elem->next;
		delete akt_elem;
	   }
	}

	List(List& old){
	   cout << "másoló konstruktor" << endl;
	   head = copy(old.head);
	}

	List(List&& old){
	   cout << "mozgató konstruktor" << endl;
	   head = move(old.head);
	   old.head = nullptr;
	}

	List& operator=(const List& old){
	   cout << "másoló értékadás" << endl;
	   head = copy(old.head);
	   return *this;
	}

	List& operator=(List&& old){
	   cout << "mozgató értékadás" << endl;
	   head = old.head;
	   old.head = nullptr;
	   return *this;
	}

	void paste(int value){
	   ListElem* nextElem = head;
	   while(nextElem->next != NULL){
		nextElem = nextElem->next;
	   }
	   ListElem* pasteElem = new ListElem();
	   nextElem->next = pasteElem;
	   pasteElem->data = value;
	   pasteElem->next = nullptr;
	}

	void print(){
	   ListElem* elem = head->next;
	   while(elem != NULL){
		   cout << elem->data << endl;
		   elem = elem->next;
	   }
	}

	void print_mem_addr(){
	   ListElem* elem = head->next;
	   while(elem != NULL){
	   	cout << elem->data << "\t" << elem << endl;
	   	elem = elem->next;
	   }
	}

	class ListElem {
	   public:
		int data = 0;
		ListElem* next = nullptr;
	};

private:
	ListElem* head = nullptr;
	ListElem* copy(ListElem* elem){
	   ListElem* newElem;
	   if(elem != NULL){
		newElem = new ListElem();
		newElem->data = elem->data;
		if(elem->next != NULL){
		   newElem->next=copy(elem->next);
		}
		else{
		   newElem->next = nullptr;
		}		
	   }
		return newElem;	
	}
};

int main(int argc, char* argv[]){
	cout << "List list; //";
	List list;

	list.paste(5);
	list.paste(3);
	list.paste(7);

	cout << "Memória címek:" << endl;
	list.print_mem_addr();

	cout << "\nList list2(list); //";
	List list2(list);	
	cout << "list2 memória címe:" << endl;
	list2.print_mem_addr();

	cout << "\nList list3; //";
	List list3;

	cout << "list3=list2; //";
	list3=list2;

	cout << "list3 memória címe:" << endl;
	list3.print_mem_addr();

	cout << "\nList list4=move(list3); //";
	List list4(move(list3));

	cout << "list4 memória címe:" << endl;
	list4.print_mem_addr();

	cout << "\nList list5; //";
	List list5;

	cout << "list5 = move(list4); //";
	list5 = move(list4);

	cout << "list5 memória címe:" << endl;
	list5.print_mem_addr();
	return 0;
}

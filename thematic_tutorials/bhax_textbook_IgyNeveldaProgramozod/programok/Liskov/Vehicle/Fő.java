
public class Fő {

	public static void main(String[] args) {
		
		Vehicle firstVehicle = new Supercar();
		firstVehicle.start();
		System.out.println(firstVehicle instanceof Car);
		Vehicle secondVehicle = (Vehicle) firstVehicle;
		secondVehicle.start();
		System.out.println(secondVehicle instanceof Supercar);
		
		//Supercar thirdVehicle = new Vehicle();
		//thirdVehicle.start();

		
		
	}
	

}

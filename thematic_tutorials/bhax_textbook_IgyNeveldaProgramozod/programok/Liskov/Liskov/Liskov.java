
public class Liskov {
	

	    public static void main(String[] args){
	        Program program = new Program();
	        Madár madár = new Madár();
	        program.függvény(madár);

	        Sólyom sólyom = new Sólyom();
	        program.függvény(sólyom);

	        Pingvin pingvin = new Pingvin();
	        program.függvény(pingvin);
	    }
	}



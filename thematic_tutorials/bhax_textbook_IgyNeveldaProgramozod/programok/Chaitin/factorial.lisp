#!/usr/bin/clisp

(defun factorial_recursive (n)
    ;;;;Ha n 0, akkor adjon vissza 1-et
    (if (= n 0)
        1
        (* n (factorial_recursive(- n 1)))))

;;;;Az első x darab faktoriálist kiiratjuk
(loop for i  from 0 to 20
    do (format t "~D! = ~D~%" i (factorial_recursive i)))
    ;;;; "~%"-vel jelöljük az új sort új sor
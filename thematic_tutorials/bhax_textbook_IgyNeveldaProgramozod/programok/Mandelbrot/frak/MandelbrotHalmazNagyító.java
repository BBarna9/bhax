public class MandelbrotHalmazNagyító extends MandelbrotHalmaz {
    private int x, y;
    private int mx, my;

    public MandelbrotHalmazNagyító(final double a, final double b, final double c, final double d, final int szélesség,
            final int iterációsHatár) {

        super(a, b, c, d, szélesség, iterációsHatár);
        setTitle("A Mandelbrot halmaz nagyításai");

        addMouseListener(new java.awt.event.MouseAdapter() {

            public void mousePressed(final java.awt.event.MouseEvent m) {

                x = m.getX();
                y = m.getY();
                if (m.getButton() == java.awt.event.MouseEvent.BUTTON1) {
                    mx = 0;
                    my = 0;
                    repaint();
                } else {
                    final MandelbrotIterációk iterációk = new MandelbrotIterációk(MandelbrotHalmazNagyító.this, 50);
                    new Thread(iterációk).start();
                }
            }

            public void mouseReleased(final java.awt.event.MouseEvent m) {
                if (m.getButton() == java.awt.event.MouseEvent.BUTTON1) {
                    final double dx = (MandelbrotHalmazNagyító.this.b - MandelbrotHalmazNagyító.this.a)
                            / MandelbrotHalmazNagyító.this.szélesség;
                    final double dy = (MandelbrotHalmazNagyító.this.d - MandelbrotHalmazNagyító.this.c)
                            / MandelbrotHalmazNagyító.this.magasság;
                    new MandelbrotHalmazNagyító(MandelbrotHalmazNagyító.this.a + x * dx,
                            MandelbrotHalmazNagyító.this.a + x * dx + mx * dx,
                            MandelbrotHalmazNagyító.this.d - y * dy - my * dy, MandelbrotHalmazNagyító.this.d - y * dy,
                            600, MandelbrotHalmazNagyító.this.iterációsHatár);
                }
            }
        });
        addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {

            public void mouseDragged(final java.awt.event.MouseEvent m) {
                // A nagyítandó kijelölt terület szélessége és magassága:
                mx = m.getX() - x;
                my = m.getY() - y;
                repaint();
            }
        });
    }

    public void pillanatfelvétel() {

        final java.awt.image.BufferedImage mentKép = new java.awt.image.BufferedImage(szélesség, magasság,
                java.awt.image.BufferedImage.TYPE_INT_RGB);
        final java.awt.Graphics g = mentKép.getGraphics();
        g.drawImage(kép, 0, 0, this);
        g.setColor(java.awt.Color.BLACK);
        g.drawString("a=" + a, 10, 15);
        g.drawString("b=" + b, 10, 30);
        g.drawString("c=" + c, 10, 45);
        g.drawString("d=" + d, 10, 60);
        g.drawString("n=" + iterációsHatár, 10, 75);
        if (számításFut) {
            g.setColor(java.awt.Color.RED);
            g.drawLine(0, sor, getWidth(), sor);
        }
        g.setColor(java.awt.Color.GREEN);
        g.drawRect(x, y, mx, my);
        g.dispose();

        StringBuffer sb = new StringBuffer();
        sb = sb.delete(0, sb.length());
        sb.append("MandelbrotHalmazNagyitas_");
        sb.append(++pillanatfelvételSzámláló);
        sb.append("_");
        // A fájl nevébe belevesszük, hogy melyik tartományban
        // találtuk a halmazt:
        sb.append(a);
        sb.append("_");
        sb.append(b);
        sb.append("_");
        sb.append(c);
        sb.append("_");
        sb.append(d);
        sb.append(".png");
        try {
            javax.imageio.ImageIO.write(mentKép, "png", new java.io.File(sb.toString()));
        } catch (final java.io.IOException e) {
            e.printStackTrace();
        }
    }

    public void paint(final java.awt.Graphics g) {

        g.drawImage(kép, 0, 0, this);
        // Ha éppen fut a számítás, akkor egy vörös
        // vonallal jelöljük, hogy melyik sorban tart:
        if (számításFut) {
            g.setColor(java.awt.Color.RED);
            g.drawLine(0, sor, getWidth(), sor);
        }
        // A jelző négyzet kirajzolása:
        g.setColor(java.awt.Color.GREEN);
        g.drawRect(x, y, mx, my);
    }
    public static void main(String[] args) {
        // A kiinduló halmazt a komplex sík [-2.0, .7]x[-1.35, 1.35]
        // tartományában keressük egy 600x600-as hálóval és az
        // aktuális nagyítási pontossággal:
        new MandelbrotHalmazNagyító(-2.0, .7, -1.35, 1.35, 600, 255);
    
    }
}

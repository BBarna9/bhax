#include <stdio.h>

int main()
{
   int a;
   int b;
   int c;

   printf("Write two numbers:\n");

   scanf("%d %d", &a, &b);

   c = a;
   a = b;
   b = c;

   printf("A = %d\n", a);
   printf("B = %d\n", b);
}
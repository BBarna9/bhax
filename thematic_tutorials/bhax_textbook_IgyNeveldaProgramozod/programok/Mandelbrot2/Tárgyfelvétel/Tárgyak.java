import java.util.Vector;
public class Tárgyak {
	
	private String type;
	private String name;
	private String coursecode;
	private int personnum;
	private int min;
	private int day;
	private int begin;
	private int end;
	private String teacher;
	private Vector<String> jelentkezettek = new Vector<String>(personnum);
	private Vector<String> neptuncodes = new Vector<String>(personnum);
	private int hely = personnum;
	public static int counter = 0;
	
	
	
	public String getTeacher() {
		return teacher;
	}
	public void setTeacher(String teacher) {
		if(teacher != null) {
			this.teacher = teacher;
		}
		else {
			System.out.println("Hibás tanárnév");
		}
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		if(type == "Köt" || type == "Kötvál" || type == "Szabvál") {
		this.type = type;}
		else {
			
			System.out.println("Hibás kurzustípus");
		}
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		if(name != null) {
			this.name = name;
		}
		else {	
			System.out.println("Hibás név");
		}
	}
	public String getCoursecode() {
		return coursecode;
	}
	public void setCoursecode(String coursecode) {
		if(coursecode != null) {
			this.coursecode = coursecode;
		}
		else {
			System.out.println("Hibás kurzuskód");
		}
	}
	public int getPersonnum() {
		return personnum;
	}
	public void setPersonnum(int personnum) {
		
		if(jelentkezettek.size()> personnum) {
			System.out.println("Nem lehetséges ennyi jelentkező");
			
		}
		if(personnum >0) {
			this.personnum = personnum;
		}
		else {
			System.out.println("Hibás létszám");
		}
	}
	public int getDay() {
		return day;
	}
	public void setDay(int day) {
		if(day > 0 && day<=7) {
			this.day = day;
		}
		else {
			System.out.println("Hibás nap");
		}	
	}
	public int getBegin() {
		return begin;
	}
	public void setBegin(int begin) {
		if(begin >=0 && begin <=24) {
			this.begin = begin;
		}
	}
	public int getEnd() {
		return end;
	}
	public void setEnd(int end) {
		if(end >= 0 && end <=24 && end != begin)
		this.end = end;
	}
	public int getMin() {
		return min;
	}
	public void setMin(int min) {
		if (min >= 0) { 
			this.min = min;
		}
		else {
			System.out.println("Hibás minimum");
		}
	}
	
	public Vector<String> getJelentkezettek() {
		return jelentkezettek;
	}
	public void setJelentkezettek(Vector<String> jelentkezettek) {
		this.jelentkezettek = jelentkezettek;
	}

	public int getHely() {
		return hely;
	}
	public void setHely(int hely) {
		this.hely = hely;
	}
	
	
	public Vector<String> getNeptuncodes() {
		return neptuncodes;
	}
	public void setNeptuncodes(Vector<String> neptuncodes) {
		
		if(neptuncodes != null) {
			this.neptuncodes = neptuncodes;
		}
		
	}
	
	public void adatok(String name, String neptuncode) {
		jelentkezettek.add(name);
		neptuncodes.add(neptuncode);
		hely --;
		counter ++;
		
		if (hely == 0) {
			System.out.println("Betelt a kurzus");
		}
	}
	
	
	public void nevkiir() {
		for (int i = 0 ; i< counter; i++) {
			
			
			System.out.println("Név: " + jelentkezettek.get(i) + "      Neptunkód: " + neptuncodes.get(i));	
		}	
		
	}
	
	public void kurzuskiir() {
		System.out.println("A tárgy neve: "+ name);
		System.out.println("Az oktató: "+teacher);
		System.out.println("A tárgy typus: "+ type);
		System.out.println("A kurzuskód: "+coursecode);
		System.out.println("A maximális létszám: "+personnum);
		System.out.println("A hét napja: "+day);
		System.out.println("Az óra kezdete: "+begin);
		System.out.println("Az óra vége: "+ end);
		System.out.println("Elérhető helyek: "+ (personnum - jelentkezettek.size()));
	}
	
	
	
}

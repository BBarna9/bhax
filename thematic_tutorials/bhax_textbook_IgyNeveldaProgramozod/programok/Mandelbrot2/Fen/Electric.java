
public class Electric extends Car {
	
	private String enginetype;
	private int timetofullycharge;
	private String co2emission;
	
	public String getEnginetype() {
		return enginetype;
	}
	public void setEnginetype(String enginetype) {
		this.enginetype = enginetype;
	}
	public int getTimetofullycharge() {
		return timetofullycharge;
	}
	public void setTimetofullycharge(int timetofullycharge) {
		this.timetofullycharge = timetofullycharge;
	}
	public String getCo2emission() {
		return co2emission;
	}
	public void setCo2emission(String co2emission) {
		this.co2emission = co2emission;
	}
	
	public void kiire() {
		
		System.out.println("A magasság: "+ getHeight()+ " cm");
		System.out.println("A súly: "+getWeight()+" kg");
		System.out.println("Az ajtók száma: "+getNumberofdoors());
		System.out.println("A befogadóképesség: "+getCapacity());
		System.out.println("A márka: "+getBrand());
		System.out.println("A lóerő: "+getHorsepower()+ " HP");
		System.out.println("A Co2 kibocsátás: "+getCo2emission());
		System.out.println("Max sebesség: "+getMaxspeed()+ " km/h");
		System.out.println("Elektromos motor típus: "+getEnginetype());
		System.out.println("Származás: "+getOrigin());
		System.out.println("Feltöltés idő: "+ getTimetofullycharge() + " min");
		
		
	}
}

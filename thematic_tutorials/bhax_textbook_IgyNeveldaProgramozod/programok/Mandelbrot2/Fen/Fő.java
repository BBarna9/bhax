
public class Fő {

	public static void main(String[] args) {
		
		Diesel diesel = new Diesel();
		diesel.setHeight(160);
		diesel.setWeight(1500);
		diesel.setNumberofdoors(4);
		diesel.setCapacity(4);
		diesel.setBrand("Mercedes");
		diesel.setHorsepower(320);
		diesel.setCo2emission("Lot");
		diesel.setMaxspeed(280);
		diesel.setDconsumption(12);
		diesel.setEnginetype("V12");
		diesel.setOrigin("Germany");
		
		
		Hybrid hybrid = new Hybrid();
		hybrid.setHeight(145);
		hybrid.setWeight(2000);
		hybrid.setNumberofdoors(4);
		hybrid.setCapacity(4);
		hybrid.setBrand("Nissan");
		hybrid.setHorsepower(300);
		hybrid.setCo2emission("Minor");
		hybrid.setMaxspeed(270);
		hybrid.setDenginetype("1.9");
		hybrid.setEenginetype("E100");
		hybrid.setOrigin("Japan");
		
		
		Electric electric = new Electric();
		electric.setHeight(130);
		electric.setWeight(2250);
		electric.setNumberofdoors(2);
		electric.setCapacity(2);
		electric.setBrand("Tesla");
		electric.setHorsepower(330);
		electric.setCo2emission("None");
		electric.setMaxspeed(290);
		electric.setEnginetype("E200");
		electric.setOrigin("USA");
		electric.setTimetofullycharge(60);
		
		diesel.kiird();
		System.out.println();
		System.out.println();
		hybrid.kiirh();
		System.out.println();
		System.out.println();
		electric.kiire();
		
	}

}

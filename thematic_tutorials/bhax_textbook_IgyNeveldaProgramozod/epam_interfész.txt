A Java 8-as verziója előtt csak absztrakt metódusokat (nem megjeleníthető osztályok, alosztályok) lehetett használni. Az interfészek metódusai alapjáraton publikusak és absztraktok. A Java 8-tól kezdődően azonban már az interfészeknek lehetnek default és statikus metódusai (külső osztályok számára nem elérhető metódusok) is, ezáltal a fejlesztők új metódusokat tudnak adni az interfészhez anélkül, hogy hatással lennének arra az adott osztályra, ami kivitelezi ezeket az interfészeket. Azonban ezek az újítások némi új problémához is vezettek. Ilyen az úgynevezett "gyémánt probléma", ami akkor keletkezik, ha egy osztály több metódust is örököl ugyanazokkal a jellemzőkkel, ami problémához vezethet a programok futásában.

https://beginnersbook.com/2017/10/java-8-interface-changes-default-method-and-static-method/

interface InterfaceDefault
{
    void abstractMethod();
     
    default void defaultMethod()
    {
        System.out.println("Ez egy default metódus");
    }
}
 
class anyClass implements InterfaceDefault
{
    @Override
    public void abstractMethod() 
    {
        System.out.println("Absztrakt metódus implementálva");
    }
     
}

Példa az absztrakt metódusra: létrehoztunk egy default interfészt, amiben deklaráltunk egy absztrakt és egy default metódust, ami tartalmaz egy kiíratást. Továbbá létrehoztunk egy osztályt és kiegészítjük a korábban létrehozott interfészzel. Ezután az interfészben létrehozott absztrakt metódust felülírjuk, ami szintén egy kiíratást fog tartalmazni, a felülírt szöveggel. Így a defaultMethod() implementálása nélkül el tudtuk érni annak tartalmát.

interface InterfaceDefaultStaticMethod
{
    void abstractMethod();    
     
    default void defaultMethod()
    {
        System.out.println("Ez egy default metódus");
    }
     
    static void staticMethod()
    {
        System.out.println("Ez egy statikus metódus");
    }
}
 
class AnyClass implements InterfaceDefaultStaticMethod
{
    @Override
    public void abstractMethod() 
    {
        System.out.println("Absztrakt metódus implementálva");
    }
     
    //No need to implement defaultMethod()
     
    //Can't implement staticMethod()
}

Példa a statikus metódusra: Ebben a kódcsipetben szintén létrehoztunk egy interfészt, amiben definiáltunk egy default és egy statikus metódust is, amikben kiíratunk egy-egy mondatot. Ezután létrehozunk egy osztályt, amit kibővítettünk az interfésszel, majd felülírtuk az interfész absztrakt metódusát. Így az osztályban már nincs szükség arra, hogy implementáljuk a defaultMethod()-ot, a staticMethod()-ot pedig nem is tudjuk megvalósítani osztályon belül.


https://javaconceptoftheday.com/java-8-interface-changes-default-methods-and-static-methods/
 